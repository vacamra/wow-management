import cfscrape
import sys
import json
import time
import functools
import mysql.connector

def connect():
	return mysql.connector.connect(
		host = 'localhost',
		database = 'WoW',
		user = 'WoW',
		password = '')

scraper = cfscrape.create_scraper()
# MaxID figured out by binary searching wowhead.com
maxId = 23328

def get_item_description(id):
	url = f"https://api.nexushub.co/wow-classic/v1/items/ignore/{id}"
	print(f"Downloading item {id}")
	content = scraper.get(url).content

	parsed = json.loads(content)

	if 'error' in parsed:
		error = parsed['error']
		print(f"Error: {error}")
		if error == 'Rate limit exceeded':
			print("waiting 5 seconds")
			time.sleep(5)
			return get_item_description(id)
		elif error == 'Not found.':
			return (True, None)

		return (False, None)

	print(f'Item {id} downloaded: {parsed["name"]}')
	return (True, parsed)


def get_starting_index(connection):
	cursor = connection.cursor()

	# max with default
	cursor.execute("SELECT MAX(`id`) FROM (SELECT `id` FROM `items` UNION SELECT 0 as `id`) AS ONE")
	id = cursor.fetchone()[0]

	cursor.close()

	return id + 1


def get_icon(iconUri):
	iconData = scraper.get(iconUri).content
	binary = bytearray(iconData)
	hexcode = ''.join('{:02x}'.format(x) for x in binary)
	return hexcode

@functools.lru_cache(maxsize=None)
def get_tag_id(connection, tagName):
	cursor = connection.cursor()
	cursor.execute("SELECT `id` FROM `tags` WHERE `name` = %s", (tagName,))
	row = cursor.fetchone()
	id = 0;
	if row == None:
		cursor.execute("INSERT INTO `tags`(`name`) VALUES (%s)", (tagName,))
		id = get_tag_id(connection, tagName)		
	else:
		id = row[0]

	cursor.close()
	return id

def getWithDefault(dictionary, key, default):
	return dictionary[key] if key in dictionary else default

def insert_item_into_db(connection, item):
	id = item['itemId']
	name = item['name']
	icon = get_icon(item['icon'])
	# these seem to not exist every time
	itemLink = getWithDefault(item, 'itemLink', f"[{name}]") 
	requiredLevel = getWithDefault(item, 'requiredLevel', 0)
	itemLevel = getWithDefault(item, 'itemLevel', 0)
	vendorSellPrice = getWithDefault(item, 'sellPrice', 0)

	cursor = connection.cursor()

	# check if the item had been inserted before
	# delete it and re-upload it if it has
	cursor.execute("SELECT `id` FROM `items` WHERE `id` = %s", (id,))
	result = cursor.fetchone()
	if result != None: 
		cursor.execute("DELETE FROM `items` WHERE `id` = %s", (id,))

	# Insert item
	sql = "INSERT INTO `items`(`id`, `name`, `requiredLevel`, `itemLevel`, `vendorSellPrice`, `itemLink`, `icon`) VALUES (%s, %s, %s, %s, %s, %s, %s)"
	args = (id, name, requiredLevel, itemLevel, vendorSellPrice, itemLink, icon)
	cursor.execute(sql, args)

	# Insert tags
	if 'tags' in item:
		for tag in item['tags']:
			if tag == None: 
				continue
			tagId = get_tag_id(connection, tag)
			cursor.execute("INSERT INTO `itemTags`(`itemId`, `tagId`) VALUES (%s, %s)", (id, tagId))

	# Insert tooltips
	if 'tooltip' in item:
		for tooltip in item['tooltip']:
			label = tooltip['label']
			format = tooltip['format'] if 'format' in tooltip else None
			sql = "INSERT INTO `tooltips`(`label`, `format`, `itemId`) VALUES (%s, %s, %s)"
			args = (label, format, id)
			cursor.execute(sql, args)

	cursor.close()
	print("DB updated")
	
def upsert_status(connection, id, status):
	cursor = connection.cursor()
	sql = """INSERT INTO `itemDownloadStatus`(`itemId`, `status`) VALUES (%s, %s) 
			ON DUPLICATE KEY UPDATE `status` = %s"""
	cursor.execute(sql, (id, status, status))
	cursor.close()


def update_item(connection, id):
	success, item = get_item_description(id)

	if success:
		if item == None:
			upsert_status(connection, id, 'does not exist')
		else:
			insert_item_into_db(connection, item)
			upsert_status(connection, id, 'ok')
	else:
		upsert_status(connection, id, 'error')

def get_retryIds(connection):
	cursor = connection.cursor()
	cursor.execute("SELECT `itemId` FROM `itemDownloadStatus` WHERE `status` = 'error'")
	rows = cursor.fetchall()
	return list(map(lambda x: x[0], rows))

def get_ids_toDownload(connection):
	if len(sys.argv) > 1:
		if sys.argv[1] == 'retry':
			return get_retryIds(connection)
		else:
			start = int(sys.argv[1])
	else:
		start = get_starting_index(connection)

	end = maxId + 1

	return range(start, end)


def download_set(connection, ids):
	successfulDownloads = 0
	for id in ids:
		# There is a download limit of 20 queries per 5 seconds)
		if successfulDownloads >= 20:
			print("Waiting 5 seconds")
			time.sleep(6)
			successfulDownloads = 0

		update_item(connection, id)
		successfulDownloads += 1

		connection.commit()


connection = connect()

ids = get_ids_toDownload(connection)
download_set(connection, ids)

connection.close()
