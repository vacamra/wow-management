import download
import query
import update
import sys

def printHelp():
	print("""
Usage: wowAddons command [addonNames]

commands:
	install - installs addons with given addonNames
	list - lists all installed addons
	update - updates the listed addons
	updateAll - updates all addons
	uninstall - uninstalls specified addons
	help - prints this help
""");

if sys.argv == 1:
	printHelp()
	exit()

command = sys.argv[1]

if command == 'install':
	if len(sys.argv) < 3:
		print("Missing arguments for install command")
		printHelp()
		exit()

	addonNames = sys.argv[2:]
	download.downloadAddons(addonNames)

elif command == 'list':
	addons = query.listAll()
	for addon in addons:
		print(addon)

elif command == 'update':
	if len(sys.argv) == 2:
		print("No argument supplied, quitting")
	else:
		update.update(sys.argv[2:])

elif command == 'updateAll':
	update.updateAll()

elif command == 'uninstall':
	if len(sys.argv) == 2:
		print("No addon name specified, quitting")
	else:
		for addon in sys.argv[2:]:
			download.removePreviousVersion(addon)

elif command == 'help':
	printHelp()

else:
	print(f"Unknown command {command}")
	printHelp()
