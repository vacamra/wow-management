import query
import download
import db

def getLastUpdated(addon):
	addonName = download.normalizeName(addon)
	
	connection = db.connect()
	cursor = connection.cursor()

	cursor.execute("SELECT lastVersion FROM addons WHERE `name` = %s", (addonName,))
	result = cursor.fetchone()[0]

	return result


def updateInternal(addonList):
	for addon in addonList:
		addonInfo = download.downloadAddonInfo(addon)
		(success, _, newVersion) = addonInfo
		lastVersion = getLastUpdated(addon)

		print(f"checking for new version of {addon}, lastVersion = {lastVersion or 'unknown'}")		

		if lastVersion == None or newVersion > lastVersion:
			print(f"new version available: {newVersion}")
			download.downloadAddon(addon, addonInfo)
		else:
			print("no new version available")

def update(addonList):
	installedAddons = query.listAll()

	toInstall = []
	for addon in addonList:
		addonName = download.normalizeName(addon)
		if addonName in installedAddons:
			toInstall.append(addon)
		else:
			print(f"addon {addon} is not installed, skipping update")

	updateInternal(toInstall)

def updateAll():
	installedAddons = query.listAll()
	updateInternal(installedAddons)
