import db

def getAddonFolder():
	connection = db.connect()
	cursor = connection.cursor()
	cursor.execute("SELECT value FROM `configuration` WHERE `key` = 'addon_folder'")
	
	result = cursor.fetchone()
	if result != None:
		return result[0]

	folder = input("addon_folder configuration value not set, select value: ")
	cursor.execute("INSERT INTO `configuration`(`key`, `value`) values (%s, %s)", ('addon_folder', folder))
	connection.commit()
	cursor.close()
	connection.close()
	return folder

