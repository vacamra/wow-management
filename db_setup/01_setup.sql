CREATE TABLE `configuration` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`key` VARCHAR(255) NOT NULL UNIQUE,
	`value` TEXT
);

INSERT INTO configuration (`key`, `value`) VALUES ('db_schema_version', '1');

CREATE TABLE `addons` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(512) NOT NULL UNIQUE
);

CREATE TABLE `addonFolders` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`folderName` VARCHAR(512) NOT NULL UNIQUE,
	`addonId` INT,
	FOREIGN KEY (`addonId`) REFERENCES `addons`(`id`) ON DELETE CASCADE
);
