CREATE TABLE `items` (
	`id` INT PRIMARY KEY,
	`name` VARCHAR(255) NOT NULL,
	`vendorSellPrice` INT NULL,
	`itemLevel` INT NOT NULL,
	`requiredLevel` INT NOT NULL,
	`itemLink` VARCHAR(255) NOT NULL,
	`icon` BLOB NOT NULL
);

CREATE TABLE `tags` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE `itemTags` (
	`itemId` INT,
	`tagId` INT,
	PRIMARY KEY (`itemId`, `tagId`),
	FOREIGN KEY (`itemId`) REFERENCES `items`(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`tagId`) REFERENCES `tags`(`id`) ON DELETE CASCADE
);

CREATE TABLE `tooltips` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`label` TEXT NOT NULL,
	`format` VARCHAR(255) NULL,
	`itemId` INT,
	FOREIGN KEY (`itemId`) REFERENCES `items`(`id`) ON DELETE CASCADE
);

CREATE TABLE `itemDownloadStatus` (
	`itemId` INT PRIMARY KEY,
	`status` VARCHAR(30) NOT NULL,

UPDATE `configuration` SET `value` = '2' WHERE `key` = 'db_schema_version';
