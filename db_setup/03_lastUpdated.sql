ALTER TABLE `addons` ADD lastVersion DateTime;

UPDATE `configuration` SET `value` = 2 WHERE `key` = 'db_schema_version';
