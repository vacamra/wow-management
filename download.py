import cfscrape
import sys
from bs4 import BeautifulSoup
import uuid
from zipfile import ZipFile
import os
import shutil
import db
import configuration
from datetime import datetime

scraper = cfscrape.create_scraper()

def normalizeName(addonName):
	return addonName.lower().replace("!", "").replace(" ", "-")

def createUrl(addonName):
	fixedName = normalizeName(addonName)
	return f"https://www.curseforge.com/wow/addons/{fixedName}/files"

def downloadAddonInfo(addonName):
	print(f"Downloading info about {addonName}")
	addonName = normalizeName(addonName)
	url = createUrl(addonName)
	htmlText = scraper.get(url).content

	html = BeautifulSoup(htmlText, 'html.parser')

	# side-bar with most current versions
	innerSidebar = html.find(class_="cf-sidebar-inner")
	if innerSidebar == None:
		print(f"\nAddon {addonName} not found!\n")
		return (False, None, None)

	found = False
	for h4 in innerSidebar.find_all('h4'):
		if not "WoW Classic" in h4.a.string:
			continue
		
		links = h4.next_sibling.next_sibling
		zip_relative_link = links.find_all("a")[1]['href']
		timestampString = links.find('abbr')['data-epoch']
		timestamp = int(timestampString)
		lastUpdated = datetime.utcfromtimestamp(timestamp)
		return (True, zip_relative_link, lastUpdated)

	print(f"\nAddon {addonName} does not have classic version")
	return (False, None, None)

def downloadAddonRaw(zip_relative_link, destinationFile):
	# the "/file" suffix skips the 'Downloading now' page
	zip_link = f"https://www.curseforge.com{zip_relative_link}/file"

	zip_content = scraper.get(zip_link).content

	with open(destinationFile, "wb") as file:
		file.write(zip_content)

	print("done")
	return True

def addAddonToDb(addonName, folderList, lastUpdated):
	print("connecting to database")

	addonName = normalizeName(addonName)

	connection = db.connect()

	if not connection.is_connected():
		print("Error connecting to DB")
		return


	cursor = connection.cursor()
	formatted_date = lastUpdated.strftime('%Y-%m-%d %H:%M:%S')
	cursor.execute("INSERT INTO addons(`name`, `lastVersion`) VALUES (%s, %s);", 
		(addonName, formatted_date))
	cursor.execute("SELECT id FROM addons where `name` = %s;", (addonName,))
	id = cursor.fetchone()[0]
	
	tuples = []
	for folder in folderList:
		tuples.append((folder, id))

	cursor.executemany(f"INSERT INTO addonFolders(`folderName`, `addonId`) VALUES(%s, %s)", tuples)

	connection.commit()
	cursor.close()
	connection.close()
	print("Successfully updated DB")

def removePreviousVersion(addonName):

	addonName = normalizeName(addonName)
	connection = db.connect()

	if not connection.is_connected():
		print("Error connecting to DB")
		return

	cursor = connection.cursor(buffered=True)
	cursor.execute("SELECT id FROM addons where `name` = %s", (addonName,))
	
	if cursor.rowcount == 0:
		return

	id = cursor.fetchone()[0]
	cursor.execute("SELECT folderName FROM addonFolders WHERE `addonId` = %s", (id,))
	for row in cursor.fetchall():
		folderName = row[0]
		if os.path.isdir(folderName):
			shutil.rmtree(folderName)

	cursor.execute("DELETE FROM addons where `id` = %s", (id,))
	connection.commit()
	cursor.close()
	connection.close()



def analyzeZip(addonName, zipPath, lastUpdated):
	with ZipFile(zipPath, 'r') as zip:
		mapFileNames = lambda file: file.filename
		isFolder = lambda file: file.endswith('/')
		removeTrailingSlash = lambda file: file[0:-1]
		isInRoot = lambda file: file.count('/') == 0

		addonDirectories = list(
			filter(isInRoot, 
			map(removeTrailingSlash, 
			filter(isFolder, 
			map(mapFileNames, zip.filelist)))))

		removePreviousVersion(addonName)
		addAddonToDb(addonName, addonDirectories, lastUpdated)
		zip.extractall()

def downloadAddon(addonName, addonInfo):
	addonFolder = configuration.getAddonFolder()
	os.chdir(addonFolder)
	
	randomFileName = str(uuid.uuid4())
	zipPath = f"/tmp/{randomFileName}"
	(success, zip_relative_path, lastUpdated) = addonInfo
	
	if success:
		downloadAddonRaw(zip_relative_path, zipPath)
		analyzeZip(addonName, zipPath, lastUpdated)
		os.remove(zipPath)
	

def downloadAddons(addonNames):
	for addonName in addonNames:
		addonInfo = downloadAddonInfo(addonName);
		downloadAddon(addonName, addonInfo)
